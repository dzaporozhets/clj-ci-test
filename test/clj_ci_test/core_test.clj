(ns clj-ci-test.core-test
  (:require [clojure.test :refer :all]
            [clj-ci-test.core :refer :all]))

(deftest a-test
  (testing "calc"
    (is (= (calc 1 2) 3))))
