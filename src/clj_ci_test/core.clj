(ns clj-ci-test.core)

(defn calc
  [x y]
  (+ x y))

(defn -main
  []
  (println "1 + 2 =" (calc 1 2)))
